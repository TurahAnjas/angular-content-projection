import { Component } from '@angular/core';

@Component({
  selector: 'button-with-projection',
  template: `
    <button>
      <ng-content></ng-content>
    </button>
  `,
})
export class ButtonWithProjectionComponent {}
