import { Component } from '@angular/core';

@Component({
  selector: 'simple-button',
  template: `<button>Click Me</button>`,
})
export class SimpleButtonComponent {}
