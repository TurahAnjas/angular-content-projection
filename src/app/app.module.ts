import { CardComponent } from './card.component';
import { ButtonWithProjectionComponent } from './button-with-projection.component';
import { SimpleButtonComponent } from './simple-button.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

@NgModule({
  imports: [BrowserModule, FormsModule],
  declarations: [
    AppComponent,
    SimpleButtonComponent,
    ButtonWithProjectionComponent,
    CardComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
